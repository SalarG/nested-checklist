import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { CheckBox, Card } from 'react-native-elements';

class CheckBoxGroup extends Component {

    render() {
        return (
            < View >
                <Card>
                    <TouchableOpacity
                        onPress={() => { this.props.onParentTitlePress(this.props.name) }}
                        style={{ backgroundColor: '#ecc0b2', padding: 15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                        <CheckBox
                            checked={this.props.checked}
                        />
                        <Text>{this.props.name}</Text>
                    </TouchableOpacity>
                </Card>
            </View >
        )
    }
}

export default CheckBoxGroup;