import React from 'react';
import { View, Text, SafeAreaView, ScrollView,StyleSheet } from 'react-native'
import _ from 'lodash';
import CheckBoxGroup from './CheckBoxGroup';
import { CheckBox } from 'react-native-elements';
import CheckBoxChild from './CheckBoxChild';

var itemList = {
    Cleaning: {
        "cleaning Apartment": ['Dishes', 'bathrooom', 'sink'],
        "clening House": ['garage', 'lawn', 'pool']
    },
    Painting: {
        "Paint inside": [],
        "Paint Outside": []

    },
    Handyman: {},
    Moving: {},
    Electrical: {},
    Plumbing: {}
};

export default class CheckList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            services: _.transform(itemList, (acc, children, groupName) => {
                acc[groupName] = {
                    name: groupName,
                    checked: false,
                    children: _.transform(children, (acc, grandchildren, childName) => {
                        acc[childName] = {
                            name: childName,
                            checked: false,
                            grandchildren: _.transform(grandchildren, (acc, grandchild) => acc[grandchild] = { name: grandchild, checked: false }, {})
                        }
                    }
                        , {})
                };
            }, {}),

            showChildren: '',
            showGrandChildren: ''
        }
    }

    onGroupChange = (groupName) => {
        let newState = _.cloneDeep(this.state);
        let group = newState.services[groupName];
        group.checked = !group.checked;
        _.forEach(group.children, child => {
            child.checked = group.checked
            _.forEach(child.grandchildren, grandchild => {
                grandchild.checked = group.checked;
            })
        });
        this.setState(newState);
    }

    onChildChange = (groupName, childName) => {
        let newState = _.cloneDeep(this.state);
        let group = newState.services[groupName];
        group.children[childName].checked = !group.children[childName].checked;
        group.checked = _.every(group.children, "checked");

        _.forEach(group.children[childName].grandchildren, grandChild => {
            grandChild.checked = group.children[childName].checked;
        })

        this.setState(newState);
    }


    onGrandChildChange = (groupName, childName, grandchildName) => {

        let newState = _.cloneDeep(this.state);

        let group = newState.services[groupName];

        group.children[childName].grandchildren[grandchildName].checked = !group.children[childName].grandchildren[grandchildName].checked;
        group.children[childName].checked = _.every(group.children[childName].grandchildren, "checked");
        group.checked = _.every(group.children, "checked");
        this.setState(newState);
    }

    onParentTitlePress = (pressedTitle) => {
        if (!this.state.showChildren) {
            this.setState({ showChildren: pressedTitle })

        }
        else {
            this.setState({ showChildren: '' })
        }
    }

    onChildTitlePress = (pressedTitle) => {
        if (!this.state.showGrandChildren) {
            this.setState({ showGrandChildren: pressedTitle })
        }
        else {
            this.setState({ showGrandChildren: '' })
        }
    }

    renderChildren = () => {
        const { showChildren } = this.state;
        if (showChildren) {
            return (
                <View style={styles.container}>
                    <CheckBox
                        title='select all'
                        onPress={() => { this.onGroupChange(showChildren) }}
                        checked={this.state.services[showChildren].checked}
                    />
                    <View>
                        {_.map(this.state.services[showChildren].children, (childItem) => {
                            return (
                                <CheckBoxChild
                                    key={childItem.name}
                                    onChildChange={this.onChildChange}
                                    onGrandChildPress={this.onGrandChildChange}
                                    {...childItem}
                                    groupName={showChildren}
                                    onChildTitlePress={this.onChildTitlePress}
                                    showGrandChildren={this.state.showGrandChildren === childItem.name}
                                />
                            )
                        })}
                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <SafeAreaView>
                <ScrollView horizontal={true}>
                    {_.map(this.state.services, (item) => {
                        return (
                            <View key={item.name}>
                                <CheckBoxGroup
                                    onGroupChange={this.onGroupChange}
                                    onChildPress={this.onChildChange}
                                    onGrandChildPress={this.onGrandChildChange}
                                    {...item}
                                    onParentTitlePress={() => { this.onParentTitlePress(item.name) }}
                                    showChildren={this.state.showChildren === item.name}
                                    renderChildren={this.renderChildren}
                                />
                            </View>
                        )
                    })}
                </ScrollView>
                <ScrollView horizontal={false} style={styles.childrenContainer}>
                    {this.renderChildren()}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    constainer:{
        flex:1
    },
    childrenContainer:{
        //marginBottom:200
    }
})