import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { CheckBox, Card } from 'react-native-elements';
import _ from 'lodash';

class CheckBoxChild extends Component {

    showGrandChildren = () => {
        if (this.props.showGrandChildren) {
            return (
                <View>
                    <Card>
                        <CheckBox
                            checked={this.props.checked}
                            onPress={() => { this.props.onChildChange(this.props.groupName, this.props.name) }}
                            title='select'
                            containerStyle={{ backgroundColor: 'transparent', borderColor: 'transparent', marginHorizontal: 0, borderBottomColor: '#787978' }}
                        />
                        {_.map(this.props.grandchildren, (grandChildItem) => {
                            return (
                                <CheckBox
                                    title={grandChildItem.name}
                                    key={grandChildItem.name}
                                    onPress={() => { this.props.onGrandChildPress(this.props.groupName, this.props.name, grandChildItem.name) }}
                                    //onPress={props.on}
                                    {...grandChildItem}
                                    groupName={this.props.name}
                                />
                            )
                        })}
                    </Card>
                </View>
            )
        }
    }
    render() {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => { this.props.onChildTitlePress(this.props.name) }}
                    style={{ backgroundColor: '#92e2ed', padding: 15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }} >
                    <CheckBox
                        checked={this.props.checked}
                    />
                    <Text>{this.props.name}</Text>
                </TouchableOpacity>
                {this.showGrandChildren()}
            </View>
        )
    }
}

export default CheckBoxChild;